var app = require('express')();
var server = require('http').createServer(app);
var io = require('socket.io').listen(server);
var chatLog = [];

server.listen(9020);

app.get('/', function (req, res) {
    res.sendfile(__dirname + '/index.html');
});

io.sockets.on('connection', function (socket) {
    var chatName = 'Anonymous';
    //console.log('here 3');
    //console.log('socket', socket);
    socket.emit('welcome', {msg: "Welcome to Nathan's Chat room"}); //i may need to add a \r\n inside the string
    socket.on('message', function(data){
        var msg = data.message.toLowerCase().trim();
        if(msg.match('^help$')){
            socket.emit('help', [
                {"command": "help", "description": "Displays a list of available commands"},
                {"command": "test: [words]", "description": "Typing 'test: ' followed by any string(s) will echo [words]."},
                {"command": "name: [chatname]", "description": "Sets the user name as [chatname] and receives a response of 'OK'."},
                {"command": "get", "description": "Gets a response of the entire contents of the chat buffer."},
                {"command": "push: [stuff]", "description": "Receives a response of 'OK'. The result is that '[chatname]: [stuff]' is added as a new line to the chat buffer."},
                {"command": "getrange [startline] [endline]", "description": "Receives a response of lines [startline] through [endline] from the chat buffer."},
                {"command": "adios", "description": "Will quit the current connection."}]);
            chatLog.push(chatName + ': ' + msg);
        }
        else if(msg.match('^test: ')){
            //if the client types 'test: ', then return the associated string
            socket.emit('post', {message: msg.slice(6)});
            //TODO: do i add this to the chat buffer?
            //chatLog.push(chatName + ': ' + msg);
            //chatLog.push(chatName + ': ' + msg.slice(6));
        }
        else if(msg.match('^name: ')){
            chatName = msg.slice(6);
            //chatLog.push(chatName + ': ' + msg);
            //chatLog.push(chatName + ': OK');
            socket.emit('post', {message: 'OK'});
            console.log('set name');
            //socket.emit('OK');
        }
        else if(msg.match('^get$')){
            socket.emit('chatLog', chatLog);
        }
        else if(msg.match('^push: ')){
            socket.emit('post', {message: 'OK'});
            chatLog.push(chatName + ': ' + msg.slice(6));
        }
        else if(msg.match('^getrange [1234567890]+ [1234567890]+$')){
            var range = msg.slice(9).split(' ');
            socket.emit('chatLog', chatLog.slice(parseInt(range[0])-1, parseInt(range[1])));
        }
        else if(msg.match('^adios$')){
            socket.disconnect();
            //console.log('client disconnected!');
        }
        else if(msg.match('^cls$')){
            chatLog = [];
        }
        else {
            chatLog.push(chatName + ': ' + msg);
            //console.log(chatName + ': ' + msg);
            socket.emit('post', {message: chatName + ': ' + msg});
        }
    });
    //client requests 'help' and send a list of commands
    /*socket.on('help', function(data){
        //TODO: return list of commands
    });*/
});